<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '/Bzp+ew,RXyaBC7]84AgqqbNw.^n2q4]spqj&6wqd@euRe{< Pp)_tj7OLAUYz{t');
define('SECURE_AUTH_KEY', '+RB1..bNpOkd3R5:f;]F)daHex=O@@zZ:tbPBd38c-[E/So  PbD]9_f1n^.(Tn]');
define('LOGGED_IN_KEY', 'W!IDqd{>[E<IA,+KNN>9/^RK0puM@}{^In{KBLh]q2,xz3s7$_k0X4%B/Q[O=%lk');
define('NONCE_KEY', '(Yd-T1:_5vBe;Df/Nx?9lLc`W!xTt7B~up3y/lC#zn puaiu3z4foDF!$}AdG-3h');
define('AUTH_SALT', 'dCvyGPH14[}D@wx*g~C-tP#*1_OaUo%~O,W~)x^?b[s[fYKUj[WJ1~(=k34)!s&;');
define('SECURE_AUTH_SALT', 'f78so7ywUMarV7Zjx4W%JH^642A.>48LUm lmiD--L9@7+KNR$8sJ$MfIU%LcGq>');
define('LOGGED_IN_SALT', 'sd@K}*e%[BQEUN}Vz=ZiMP:{)ji[?ig93C&.PsZ,.;zz&%NSDawSUx5eUgt5H+mb');
define('NONCE_SALT', 'mte&5T;cW]1THnNBWE>cr_1LvHH1HK1O$^b7x|vAP.w$Kv+w&nQA|*T?Xf(^zXiX');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

